(function(window, $, undefined) {
	"use strict";

	// Import globals
	var Kat = window.Kat, Backbone = window.Backbone, Marionette = Backbone.Marionette, _ = window._;


	/**
	 * User application
	 * ----------------------
	 * This part takes care of binding the User UI (whaaat?!) and
	 * defining methods to interact with the server
	 *
	 * @author Carl OGREN
	 */
	Kat.Users = (function() {
		// Create empty object for app
		var Users = {};









		/**
		 * ==============================================================================
		 * 
		 *            BACKBONE COLLECTIONS, VIEWS, MODELS FOR THE USER APP
		 * 
		 * ==============================================================================
		 */



		/**
		 * User model
		 */
		var User_model = Backbone.Model.extend({
			"urlRoot": "/users",


			/**
			 * Method called by backbone change events on the model to validate the content of the values
			 *
			 * @this {User_model}
			 * @param  {array} attrs     The attributes that have changed
			 * @param  {object} options
			 * @return {Boolean}         True if the validation passes else false
			 */
			"validate": function(attrs, options) {
				
			}
		});

		// test data
		window.User_model = User_model;


		/**
		 * Users collection
		 * @type {Backbone.Collection}
		 */
		var User_collection = Backbone.Collection.extend({
			"url": "/users/list",
			"model": User_model,
			"comparator": function(user) {
				return user.get("firstname");
			}
		});
















		/**
		 * ==============================================================================
		 * 
		 *                          PRIVATE API OF THE USER APP
		 * 
		 * ==============================================================================
		 */
















		/**
		 * ==============================================================================
		 * 
		 *                         PUBLIC API OF THE USER APP
		 * 
		 * ==============================================================================
		 */

		/**
		 * Initialize the User app
		 */
		Users.init = function() {
			Kat.log("[Kat.Users.init] Initializing User app", Kat.log.DEBUG);

			if (typeof window.localStorage !== "unknown") {
				// Inspect window.location.hash in check if user is logged in
				if (window.location.hash.length !== 0 && Kat.data.logged_in === false) {
					// store window.location.hash to re-apply later
					window.localStorage.setItem("next", window.location.hash);
					
					// redirect to login page
					_.defer(function() {
						Kat.log("****** Displaying the login form! w00t", Kat.log.DEBUG);
						Kat.vent.trigger("Kat.login:display_login_form");
					});
				} else if (window.localStorage.getItem("next") !== null && window.localStorage.getItem("next") !== "" && Kat.data.logged_in === true) {
					window.location.hash = window.localStorage.getItem("next");
					window.localStorage.removeItem("next");
				}
			}
		};


		/**
		 * Retrieve an instance of the User_model
		 * @param  {int} user_id  The user's id
		 * @return {User_model}   Instance of the User_model class
		 */
		Users.get = function(user_id) {
			if (user_id === undefined) {
				return new User_model();
			} else {
				return new User_model({ "id": user_id });
			}
		};



		Users.get_collection = function(options) {
			return new User_collection(options);
		};
		
		



		

		// Return the Users object
		return Users;
	})();



	/**
	 * Bind initializer for User app
	 */
	Kat.addInitializer(function() {
		Kat.Users.init();
	});


})(window, jQuery);