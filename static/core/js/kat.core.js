(function(window, $, undefined) {
	"use strict";


	/**
	 * Kat controller - Main namespace for the whole system
	 */
	window.Kat = new Backbone.Marionette.Application();


	// Renderer with dust.js
	Backbone.Marionette.Renderer.render = function(template, data) {
		var html = "";

		// console.log(template);

		dust.render(template, data, function(err, out) {
			if (err) {
				console.error(err);
			} else {
				html = out;
			}
		});
		return html;
	};


	/**
	 * Log information to the console
	 * 
	 * @param  {string} msg  The message to be logged
	 * @param  {mixed} type The type of log
	 */
	Kat.log = function(msg, type) {
		type = type || this.log.level;

		var level = typeof type === 'number' ? type : this.log[type.toUpperCase()];

		if (level > this.log.level) { return; }
		else {
			type = this.log.levels[level - 1] || "log";

			// Retrieve current date
			var date = new Date();
			msg = "[" + date.format("isoDate") + " " + date.format("isoTime") + "] [" + type.toUpperCase() + "] " + msg;

			// Log message to available console
			if (window.console) {
				if (window.console[type]) {
					if (window.console[type].call) {
						window.console[type].call(window.console, msg);
					} else {
						window.console[type](msg);
					}		
				} else if (window.console.log) {
					if (window.console.log.call) {
						window.console.log.call(window.console, msg);
					} else {
						window.console.log(msg);
					}
				}
			} else if (window.opera && window.opera.postError) {
				if (window.opera.postError.call) {
					window.opera.postError.call(window.opera, msg);
				} else {
					window.opera.postError(msg);
				}
			}
		}
	};


	/**
	 * return the object in consol log
	 * @author Diono
	 * @param  {object} obj the object that it will returned
	 * @return {log}     the object
	 */
	Kat.log.obj = function(obj) {
		if (obj instanceof Array){
			for(var i = 0, obj_length = obj.length; i < obj_length; i++) {
				log.obj(obj[i]);
			}
		} else {
			if (window.console) {
			} else if (window.opera && opera.postError && JSON) {
				opera.postError(JSON.stringify(obj));
			}
		}
	};

	// Log string types
	Kat.log.levels = ['error', 'warn', 'info', 'debug'];

	// Log const types
	Kat.log.ERROR = 1;
	Kat.log.WARN = 2;
	Kat.log.INFO = 3;
	Kat.log.LOG = 3;
	Kat.log.DEBUG = 4;

	// Current log level
	Kat.log.level = Kat.log.DEBUG;

	// Libs holder
	Kat.Libs = {};

	// Language object
	Kat.lang = {};


	/**
	 * Modal region specification
	 *
	 * @type {Backbone.Marionette.Region}
	 */
	var ModalRegion = Backbone.Marionette.Region.extend({
		el: "#modal",

		constructor: function(){
			_.bindAll(this);
			Backbone.Marionette.Region.prototype.constructor.apply(this, arguments);
			this.on("show", this.showModal, this);
		},

		getEl: function(selector){
			var $el = $(selector);
			$el.on("close", this.close);
			return $el;
		},

		showModal: function(view){
			var self = this;

			view.on("close", this.hideModal, this);
			this.$el.dialog(_.extend(view.dialog_options || {}, { "autoOpen": false }));
			this.$el.dialog('open');
		},

		hideModal: function(){
			this.$el.dialog('close');
		}
	});


	// Add application regions
	Kat.addRegions({
		"modal": ModalRegion
	});


	// Start Backbone.Marionette application
	$(function() {

		Kat.log("[Kat.start] Starting Kat Backbone.Marionette application", Kat.log.DEBUG);
		Kat.start();

	});


})(window, jQuery);