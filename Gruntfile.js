module.exports = function(grunt) {

	// debug_mode mode
	var debug_mode = true;

	var _static = "static/",
		_core = _static + "core/",
		_libs = _static + "libs/",
		_mods = _static + "mods/";




	// Project configuration
	grunt.initConfig({
		// Retrieve package.json file for project info
		"pkg": grunt.file.readJSON("package.json"),

		

		/**
		 * ====================================================================
		 * 
		 *               Validate javascript syntax with jshint
		 *
		 * ====================================================================
		 */
		"jshint": {
			"all": [
				"Gruntfile.js",

				// Core
				_core + "**/*.js",

				// Libs

				// Mods
				_mods + "**/*.js"
			]
		},



		/**
		 * ====================================================================
		 * 
		 *              Concatenate javascript and css files
		 *
		 * ====================================================================
		 */
		"concat": {
			// Concatenation options
			"options": {
				"stripBanners": true,
				"banner": '/**\n * <%= pkg.name %> - v<%= pkg.version %> - ' +
					'<%= grunt.template.today("yyyy-mm-dd") %>\n' +
					' * <%= pkg.repository.url %>\n' +
					' * Free Beer License - \\o/ \n */\n'
			},

			// JS loaded globally
			"js_core": {
				"src": [
					// Libs
					"static/libs/jquery/js/jquery.1.10.2.min.js",
					"static/libs/jquery/js/jquery-migrate-1.2.1.min.js",

					"static/libs/backbone/underscore-min.js",
					"static/libs/backbone/backbone-min.js",
					"static/libs/backbone/backbone.marionette.min.js",

					"static/libs/bootstrap/js/bootstrap.min.js",

					"static/libs/dust/js/dust-core-2.2.0.js",

					"static/sys/dust/kat.dust.js",

					// CM core
					"static/core/js/kat.common.js",
					"static/core/js/kat.core.js",
					"static/core/js/kat.users.js"
				],
				"dest": "static/sys/js/kat.core.js"
			}

			// // JS loaded only when not logged in
			// "js_not_logged_in": {
			// 	"src": [
			// 		// login module
			// 		'static/mods/login/js/*.js',

			// 		// inscription module
			// 		'static/mods/inscription/js/*.js'
			// 	],
			// 	"dest": "static/cm.public.js"
			// },

			// // JS loaded when logged in
			// "js_logged_in": {
			// 	"src": [
			// 		// exchange module
			// 		'static/mods/exchange/js/*.js'

			// 		// etc
			// 	],
			// 	"dest": "static/cm.modules.js"
			// },

			// // CSS files
			// // @todo separate in `not logged` and `logged in` categories
			// "css": {
			// 	"src": [
			// 		// CM core css
			// 		'static/core/css/*.css'
			// 	],
			// 	"dest": 'static/cm.core.css'
			// }
		},



		/**
		 * ====================================================================
		 *
		 *            Clean directories and files for production
		 *    Only execute this task if absolutely sure of what you're doing
		 *
		 * ====================================================================
		 */
		"clean": {
			"options": { "no-write": debug_mode },

			"js": {
				"src": [_core, _libs, _mods]
			},
			"data": {
				"src": [_static + "data/*"]
			}
		},


		/**
		 * ====================================================================
		 * 
		 *              Minify concatenated javascript files
		 *
		 * ====================================================================
		 */
		"uglify": {
			"options": {
				"mangle": false
			},

			// JS Core
			"js_core": {
				"files": {
					"static/sys/js/kat.core.min.js": ["static/sys/js/kat.core.js"]
				}
			}
		},



		/**
		 * ====================================================================
		 * 
		 *                Minify concatenated css files
		 *
		 * ====================================================================
		 */
		"cssmin": {
			"options": {
				"report": 'gzip'
			},
			"minify": {
				"src": ["static/cm.core.css"],
				"dest": 'static/cm.core.min.css'
			}
		},



		/**
		 * ====================================================================
		 *
		 *                 Dustjs template compilation
		 *
		 * ====================================================================
		 */
		"dust": {
			// Task compilation
			"kat": {
				// Source files
				// ------------
				// Finds all the dust files in the static folder and compiles
				// them into one file in the static/build/dust folder
				"files": [
					{
						"expand": true,
						"cwd": "static",
						"src": ["**/*.dust"],
						"dest": "static/sys/dust",
						"ext": ".dust.js",
						"flatten": true
					}
				],

				// Dust compilation options
				"options": {
					"wrapper": false,
					"runtime": false,
					"useBaseName": true,
					"relative": true
				}
			}
		},

		

		/**
		 * ====================================================================
		 * 
		 *           Watch added/removed files and re-execute tasks
		 *
		 * ====================================================================
		 */
		"watch": {
			"files": ["<%= jshint.all %>", "static/**/*.dust"],
			"tasks": ["dust", "jshint"]
		}
	});

	// Load plugins
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks("grunt-contrib-concat");
	grunt.loadNpmTasks("grunt-contrib-uglify");
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.loadNpmTasks("grunt-contrib-clean");
	grunt.loadNpmTasks("grunt-dust");

	// Declare default task
	grunt.registerTask("default", ["jshint"]);
};