<?php
/**
 * User model english lang file
 */

$lang['user_id'] 			= "ID";
$lang['user_username'] 		= "Username";
$lang['user_password'] 		= "Password";
$lang['user_created_at'] 	= "Creation date";
$lang['user_updated_at'] 	= "Update date";
$lang['user_deleted_at'] 	= "Delete date";
$lang['user_deleted'] 		= "Deleted";