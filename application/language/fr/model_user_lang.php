<?php
/**
 * User model english lang file
 */

$lang['user_id'] 			= "ID";
$lang['user_username'] 		= "Nom d'utilisateur";
$lang['user_password'] 		= "Mot de passe";
$lang['user_created_at'] 	= "Date de création";
$lang['user_updated_at'] 	= "Date de mise à jour";
$lang['user_deleted_at'] 	= "Date de suppression";
$lang['user_deleted'] 		= "Supprimé";