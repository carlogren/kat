<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="Aalma Dili">
	<meta name="author" content="Carl OGREN">
	<meta name="keywords" content="">

	<title>Florence Gobicchi : Admin</title>

	<!-- Stylesheets -->
	<link rel="stylesheet" type="text/css" href="/static/libs/jquery/css/smoothness/jquery-ui-1.8.16.css" />
	<link rel="stylesheet" type="text/css" href="/static/mods/admin/core/css/fg.admin.css" />
	<link rel="stylesheet" type="text/css" href="/static/mods/admin/mods/menu/css/fg.admin.menu.css" />

	<!-- JavaScripts -->
	<?php $this->load->view('templates/javascripts'); ?>

	<script type="text/javascript">
		(function() { FG.data = <?php echo json_encode($json_data); ?>; })();
	</script>

	<link rel="shortcut icon" href="" />
</head>
<body>
	<div id="content"></div>
</body>
</html>