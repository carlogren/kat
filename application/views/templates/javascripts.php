<?php 


function check_current_page($pages, $current_page) {
	$pages = explode(",", $pages);

	foreach ($pages as $page) {
		if ($page == $current_page) {
			return true;
		}
	}

	return false;
}


?>

<?php foreach ($this->config->item('javascripts') as $section_name => $section_data): ?>
	<?php if (check_current_page($section_name, $current_page)): ?>
		<?php foreach ($section_data as $script): ?>
			<?php if (!is_array($script)): ?>
				<script type="text/javascript" src="<?php echo $script ?>"></script>
			<?php else: ?>
				<script type="text/javascript" <?php foreach ($script as $attr => $val) { echo $attr . '="' . $val . '" '; } ?>></script>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif; ?>
<?php endforeach; ?>