<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<title>Kat Bootstrap</title>

<!-- Stylesheets -->
<?php echo $this->load->view('templates/stylesheets') ?>


<!-- JavaScripts -->
<?php echo $this->load->view('templates/javascripts') ?>


<link rel="shortcut icon" href="" />
