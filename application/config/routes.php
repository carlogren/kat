<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/



/**
 * CodeIgniter routes
 */
$route['default_controller'] = "pages/view";
// $route['404_override'] = '';


// $route["test"] = "pages/test";
// $route["test/create"] = "pages/test_create";
// $route["test/login"] = "pages/test_login";

/**
 * Admin routes
 * ------------
 * Basically just the login form
 * The admin system is integrated into the website itself
 */
$route['admin'] 		= "admin/index";



/**
 * User routes
 * -----------
 * Redirects calls to handle user requests
 */
$route['users/login']			= "users/login";
$route['users/logout']			= "users/logout";

$route['users/list']			= "users/list";
$route['users/(:any)']			= "users/user_handle/$1";
$route['users']					= "users/user_handle";





/* End of file routes.php */
/* Location: ./application/config/routes.php */