<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');



$config['stylesheets'] = array(
	'http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600',

	// LIBS . '/jquery/css/smoothness/jquery-ui-1.8.16.css',

	LIBS . '/bootstrap/css/bootstrap.css',
	LIBS . '/bootstrap/css/bootstrap-theme.css',
	LIBS . '/bootstrap/css/bootstrap-responsive.css',

	//CORE
	CORE_CSS . '/kat.justified-nav.css'
);