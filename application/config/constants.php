<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');



/*
|---------------------------------
| RouxLibres constants
|---------------------------------
|
| These constants define the RouxLibres environment options
|
*/

/* Urls */

define('STATIC_PATH', '/static');

/**
 * Core paths
 */
define('CORE', STATIC_PATH . '/core');

define('CORE_CSS', CORE . '/css');
define('CORE_JS', CORE . '/js');

define('BUILD', STATIC_PATH . '/build');

/**
 * Libs paths
 */
define('LIBS', STATIC_PATH . '/libs');


/**
 * Modules paths
 */
define('MODS', STATIC_PATH . '/mods');

define('ADMIN', MODS . '/admin');
define('ADMIN_CORE', ADMIN . '/core');
define('ADMIN_MODS', ADMIN . '/mods');


define('IMAGES', STATIC_PATH.'/images');

define('MEDIAS', STATIC_PATH.'/medias');

define('VIEWS', APPPATH.'/views');

/* End of file constants.php */
/* Location: ./application/config/constants.php */