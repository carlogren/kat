<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');



$config['javascripts'] = array(
	"main,admin" => array(
		// jQuery scripts
		array("src" => LIBS . '/jquery/js/jquery.1.10.2.min.js'),
		array("src" => LIBS . '/jquery/js/jquery-migrate-1.2.1.min.js'),
		
		// Dust.js
		array("src" => LIBS . '/dust/js/dust-core-2.2.0.js'),

		// Underscore + Backbone + Marionette
		array("src" => LIBS . '/backbone/js/underscore-min.js'),
		array("src" => LIBS . '/backbone/js/backbone-min.js'),
		array("src" => LIBS . '/backbone/js/backbone.marionette.min.js'),

		//Twitter bootstrap
		array("src" => LIBS . '/bootstrap/js/bootstrap.min.js'),

		// Compiled dust
		array("src" => BUILD . '/dust/kat.dust.js'),

		// Core
		array("src" => CORE_JS . '/kat.common.js'),
		array("src" => CORE_JS . '/kat.core.js'),
		array("src" => CORE_JS . '/kat.users.js'),

		// Libs
		

		// Modules
		
	),

	"main" => array(
		
	),

	"admin" => array(
		// Admin Core Scripts
		

		// Admin Libraries
		
		
		// Admin modules
	),
	
);