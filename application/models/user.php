<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * User model
 * ----------
 * Represents the `users` table in the database,
 * takes care of all the CRUD operations and data validations.
 * 
 * @extends DataMapper
 * @author Carl OGREN <carl.ogren@gmail.com>
 */
class User extends DataMapper {

	// Configure modification timestamp column names
	var $created_field = "created_at";
	var $updated_field = "updated_at";


	// Force system to use local time
	var $local_time = true;


	// Declare model relationships
	// var $has_many = array('book');
	// var $has_one = array('country');


	// Model field validation rules
	// see http://datamapper.wanwizard.eu/pages/validation.html for more details
	var $validation = array(
		'email' => array(
			'field' => 'email',
			// 'label' => 'Username',
			'rules' => array('required', 'trim', 'unique', 'valid_email', 'min_length' => 3, 'max_length' => 128),
		),
		'password' => array(
			'field' => 'password',
			// 'label' => 'Password',
			'rules' => array('required', 'min_length' => 6, 'encrypt'),
		),
		// 'confirm_password' => array(
		//     'label' => 'Confirm Password',
		//     'rules' => array('required', 'encrypt', 'matches' => 'password'),
		// ),
		// 'email' => array(
		//     'label' => 'Email Address',
		//     'rules' => array('required', 'trim', 'valid_email')
		// )
	);


	/**
	 * Validate a users credentials
	 * 
	 * @return boolean True if the user was uathenticated else false
	 */
	function login() {
		// Create a temporary user object
		$u = new User();

		// Get this users stored record via their username
		$u->where('email', $this->username)->get();

		// Give this user their stored salt
		$this->salt = $u->salt;

		// Validate and get this user by their property values,
		// this will see the 'encrypt' validation run, encrypting the password with the salt
		$this->validate()->get();

		// If the username and encrypted password matched a record in the database,
		// this user object would be fully populated, complete with their ID.

		// If there was no matching record, this user would be completely cleared so their id would be empty.
		if ($this->exists()) {
			// Login succeeded
			return TRUE;
		} else {
			// Login failed, so set a custom error message
			$this->error_message('login', 'Email or password invalid');

			return FALSE;
		}
	}


	/**
	 * Validation prepping function to encrypt passwords
	 * If you look at the $validation array, you will see the password field will use this function
	 */
	function _encrypt($field)
	{
		// Don't encrypt an empty string
		if (!empty($this->{$field}))
		{
			// Generate a random salt if empty
			if (empty($this->salt)) {
				$this->salt = md5($this->{$field});
			}

			$this->{$field} = sha1($this->salt . $this->{$field});
		}
	}
}

/* End of file user.php */
/* Location: ./application/models/user.php */