<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|---------------------------
| Users controller
|---------------------------
|
| Exposes interfaces to control, retrieve and modify user data
|
*/
class Users extends CI_Controller {
	
	public function __construct() {
		parent::__construct();

		if (!isset($_SESSION['logged_in'])) {
			$_SESSION['logged_in'] = false;
		}

		// load helpers
		$this->load->helper('url');
	}



	public function login() {
		$u = new User();

		// retrieve params
		$u->username = $this->input->post('username');
		$u->password = $this->input->post('password');

		// Store user data in session
		// $_SESSION['login_data'] = $u->data;

		// Check if user entered the correct password
		if ($u->login()) {
			log_message("debug", "[Aalma Dili] User connected with username: " . $u->username);

			// Mark user as logged in
			$_SESSION['logged_in'] = true;

			// Delete login errors
			$_SESSION['login_error'] = null;
		} else {
			log_message("error", "[Controllers.users] Username: " . $u->username . ". Error message:" . $u->error->login);

			$_SESSION['login_error'] = "Combinaison login / mdp non-valide";
		}

		// Redirect to admin page
		redirect("/admin");
	}



	public function logout() {
		if (!$_SESSION['logged_in']) {
			log_message('debug', '[Aalma Dili] User is not currently logged in');
		} else {
			$_SESSION['logged_in'] = false;
			$_SESSION['login_data'] = null;
		}

		// redirect to homepage
		redirect('/admin');
	}


	/**
	 * Receive user manipulation commands to read, create, update and delete a user
	 * This method is called from the `/user[/(user_id)]` route
	 * 
	 * @param int $user_id The user's id
	 */
	public function user_handle($user_id = false) {
		// Control that user is logged in
		if (!$_SESSION['logged_in']) {
				log_message("error", "[users.action_route] Attempt access user data without being logged in");
				show_error("You must be logged in to perform this action", 403);
		} else {
			// Retrieve request method
			$req = $_SERVER["REQUEST_METHOD"];

			// Route call according to request method
			if ($req == "GET") {                            // read
				// $this->user_lib->read($user_id);
			} elseif ($req == "POST") {                     // create
				$this->user_lib->create();
			} elseif ($req == "PUT" || $req == "PATCH") {   // update
				$this->user_lib->update($user_id);
			} elseif ($req == "DELETE") {                   // delete
				$this->user_lib->delete($user_id);
			} else {
				show_error("An error occured while fetching the data", 500);
			}
		}
	}

}


?>