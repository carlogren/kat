<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|---------------------------
| Pages controller
|---------------------------
|
| Loads the main pages of the site using the base view
|
*/
class Pages extends CI_Controller {

	// Load common functionalities
	public function __construct() {
		parent::__construct();

		if (!isset($_SESSION['logged_in'])) {
			$_SESSION['logged_in'] = false;
		}
	}


	
	// Default controller
	public function view($page = 'main', $subpage = null) {
		// load helpers & libraries
		$this->load->helper('form');

		$data['current_page'] = $page;

		// Render base view
		$this->load->view('base', $data);
	}


	public function test() {
		$user = new User();

		$user->get();

		$json = array();

		$i = 0;
		foreach ($user->all as $user) {
			$json[$i] = $user->stored;
			$i++;
		}

		echo json_encode($json);

		// echo $user->email;
	}


	public function test_create() {
		log_message("debug", "Creating user !");

		$user = new User();

		$user->email = "carl.ogren@gmail.com";
		$user->password = "R@stamaN78";



		if ($user->save()) {
			echo "ID : " . $user->id . "<br>";
			echo "Email : " . $user->email . "<br>";
			echo "Password : " . $user->password . "<br>";
		} else {
			echo $user->error->email . "<br>";
			echo $user->error->password . "<br>";
		}

		// $test_user = new User();

		// $test_user->username = "shnin";
		// $test_user->password = "rastaman";

		// if ($test_user->login()) {
		// 	echo "w00t successfully validated login<br>";
		// } else {

		// 	echo $test_user->password."<br>";
		// 	echo "Could not authenticate user<br>";
		// }


		// $user->delete();
	}


	public function test_login() {
		$user = new User();

		$user->email = "carl.ogren@gmail.com";
		$user->password = "R@stamaN78";

		if ($user->login()) {
			echo "w00t logged in !";
		} else {
			echo "blaaaah";
		}
	}

}

?>